/*
 * Express middleware to keep track of sessions
 */
var JWT    = require('jsonwebtoken');
var Bcrypt = require('bcryptjs');

var Session = require('../vnu_modules/session.js');
var Logger = require('../vnu_modules/logger.js');
var VnuUtil = require('../vnu_modules/vnuutil.js');
var Config = require('../config');

var debugTag = "SessionMiddleware";

module.exports = function(req, res, next) {
    res.saveAndSend = function (json) {
        Logger.debug(debugTag, "Save and send called");
        Logger.debug(debugTag, json);
        req.session.save().then(function() {
            Logger.debug(debugTag, "Save and send session saved");
            if (typeof json !== 'object') json = {};
            res.json(json);
        }).catch(next);
    }

    // decode token
    var sessionTokenString = req.body.token || req.query.token || req.headers['x-access-token'];
    if (sessionTokenString) {
        // verifies secret and checks
        JWT.verify(sessionTokenString, Config.secret, function(err, decoded) {
            if (err) {
                Logger.error(debugTag, "Error verifying jsonwebtoken"+JSON.stringify(err));
                return next(new VnuUtil.ErrorWithStatus("Failed to verify token", 401));
            } else {
                // if everything is good, save to request
                Logger.debug(debugTag, "token verified");
                Logger.debug(debugTag, decoded);
                req.decodedToken = decoded;

            }
        });
    } else {
        // if there is no token in the request
        return next(new VnuUtil.ErrorWithStatus("No session token provided", 401));

    }


    var sessionId = req.decodedToken.sessionId

    // retrieve session from database
    if (sessionId) {

        req.session = new Session(sessionId)

        req.session.getFromDatabase().then(function(session) {
            if (session.state === Session.GOOD) {
                req.user = session;
                Logger.debug(debugTag, "req.user="+JSON.stringify(req.user))
                return next();
            } else {
                Logger.error(debugTag, "Session.state is not good("+Session.GOOD+" session="+JSON.stringify(req.user));
                return next(new VnuUtil.ErrorWithStatus("Session Invalid", 401));
            }

        }).catch(next);

    } else {
        Logger.error(debugTag, "req.decodedToken.sessionId is not set req.decodedToken="+JSON.stringify(req.decodedToken))
        return next(new VnuUtil.ErrorWithStatus("Invalid access token", 401));
    }
}
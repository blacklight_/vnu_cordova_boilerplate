
var CassandraDriver = require('cassandra-driver');
var cassandra = global.cassandra;

function Act(args) {
    // TODO: add validation for args

    this.id = args.id;                           // timeuuid identifier for the act
    this.regionid = args.regionid;
    this.admins = args.admins;                           // list of users that have access to this act
    this.name = args.name;                           // Act name as a string

    if (args.hasOwnProperty('data')) {
        this.avatar = args.data.avatar;
        this.genre = args.data.genre;
    } else {
        this.avatar = args.avatar;                   // image avatar
        this.genre = args.genre;                            // genre as string for now, create as objects later
    }

    this._class = this.constructor.name;
}

Act.COLUMNFAMILY = "acts";
Act.CLASSNAME = "Act";

Act.prototype.constructor.name = Act.CLASSNAME;

Act.prototype.save = function() {
    const self = this;
    return new Promise(function(fulfill, reject) {
        // make a copy of the object to pass into the cassandra query
        // the data parameter is a map of parameter strings in the cassandra db that are not finalized enough to move to it's own column yet
        const copy = Object.assign({
            data: {
                avatar: self.avatar,
                genre: self.genre
            }
        }, self);

        // TODO: interpret the cassandra call output
        console.log("saving act ", self);
        cassandra.execute('INSERT INTO '+Act.COLUMNFAMILY+' (id, regionid, name, admins, data, datemodified) VALUES ' +
            '(:id, :regionid, :name, :admins, :data, toTimestamp(now()))', copy, { prepare: true }).then(function() {
            fulfill(self);
        }).catch(reject);
    });
}


/*
 * Static methods
 */

Act.getByID = function(id) {
    const idAsTimeUUID = CassandraDriver.types.TimeUuid.fromString(id.toString());
    return new Promise(function (fulfill, reject) {
        cassandra.execute('SELECT * from acts WHERE id = ?', [idAsTimeUUID]).then(function(resultSet) {
            //console.log("act row: ", resultSet.rows[0]);
            var act = new Act(resultSet.rows[0]);
            fulfill(act);
        }).catch(reject);
    });
}
module.exports = Act;
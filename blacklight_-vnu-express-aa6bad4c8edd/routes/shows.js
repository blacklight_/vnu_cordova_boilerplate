const express = require('express');
const router = express.Router();

const Factory = require('../vnu_modules/factory.js');
const Show = require('../vnu_modules/show');
const Act = require('../vnu_modules/act');



router.get('/recent', function(req, res, next) {
    Show.getRecentShows().then(function (shows) {
        console.log("got shows ", shows);
        //res.render('index', { title: 'Shows' , msg: shows.toString()})
        res.json(shows);
    }).catch(next)
});

module.exports = router;
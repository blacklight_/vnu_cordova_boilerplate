var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.json("Users Index page");
});

/*
 * Retrieve basic info for the logged in user
 */
router.get('/me', function(req, res, next) {
    // Skeleton function

    const userInfo = {username: 'testusername'};

    res.json(userInfo)
});
module.exports = router;

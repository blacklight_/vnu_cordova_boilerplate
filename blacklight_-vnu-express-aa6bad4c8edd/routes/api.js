const express = require('express');
const router = express.Router();



router.get('/test', function(req, res, next) {
    Factory.generateDemoData().then(result =>

        res.render('index', { title: 'Express' })
    ).catch(next);

});

router.get('/recentshows', function(req, res, next) {
    Show.getRecentShows().then(function (shows) {
        console.log("got shows ", shows);
        //res.render('index', { title: 'Shows' , msg: shows.toString()})
        res.json(shows);
    }).catch(next)
});


module.exports = router;
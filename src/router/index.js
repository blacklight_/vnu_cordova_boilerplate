import Vue from 'vue'
import Router from 'vue-router'
import store from '../vuex/store'

import Favourites from '../components/Favourites'
import UserProfile from '../components/UserProfile'
import EventList from '../components/EventList'
import ModalWindow from '../components/ModalWindow'
import MainLayout from '../components/MainLayout'
import OpenedObject from '../components/OpenedObject'
import VnuConnect from '../components/VnuConnect'
import VnuConnectSub from '../components/VnuConnectSub'
import Login from '../components/Login'
import Search from '../components/Search'
import Settings from '../components/Settings'

Vue.use(Router)

const routes = [
  {
    path: '/',
    redirect: '/mainLayout'
  },
  {
    path: '/mainLayout',
    name: 'MainLayout',
    components: {
      default: MainLayout,
      mainModal: ModalWindow
    },
    children: [
      {
        path: '',
        redirect: '/eventList'
      },
      {
        path: '/eventList',
        name: 'EventList',
        components: {
          default: EventList,
          openedObjectModal: OpenedObject
        }
      },
      {
        path: '/favourites',
        name: 'Favourites',
        component: Favourites
      },
      {
        path: '/userProfile',
        name: 'UserProfile',
        component: UserProfile
      },
      {
        path: '/settings',
        name: 'Settings',
        component: Settings
      }
    ]
  },
  {
    path: 'vnuconnect',
    name: 'VnuConnect',
    component: VnuConnect,
    children: [
      {
        path: 'vnuconnectsub',
        name: 'VnuConnectSub',
        component: VnuConnectSub
      },
      {
        path: 'login',
        name: 'Login',
        component: Login
      },
      {
        path: 'search',
        name: 'Search',
        meta: { AuthUser: true, AuthHost: true },
        component: Search
      }
    ]
  }
]

const routeObj = new Router({
  routes
})

routeObj.beforeEach((to, from, next) => {
  if (to.meta.AuthUser && !store.state.auth.userStatus) {
    next({ name: 'Login' })
  } else if (to.meta.AuthHost && !store.state.auth.hostStatus) {
    next({ name: 'VnuConnectSub' })
  } else {
    next()
  }
})

export default routeObj

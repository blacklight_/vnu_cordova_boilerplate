import axios from 'axios'
import { store } from '../vuex/store'

export default function setup () {
  axios.interceptors.request.use(function (config) {
    const token = store.getters.token
    if (token) {
      config.headers = { 'x-access-token': token }
    }
    return config
  }, function (err) {
    return Promise.reject(err)
  })
}

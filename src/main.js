import Vue from 'vue'
// import App from './App'
import NewVue from './newVue'
import router from './router'
import store from './vuex/store'
import * as VueGoogleMaps from 'vue2-google-maps'
import VeeValidate from 'vee-validate'

import VueCordova from 'vue-cordova'
Vue.use(VueCordova, {
  optionTestKey: 'optionTestValue'
})

import Vuetify from 'vuetify'

// import interceptorsSetup from 'src/helpers/interceptors'
// interceptorsSetup()

// add cordova.js only if serving the app through file://
if (window.location.protocol === 'file:' || window.location.port === '3000') {
  var cordovaScript = document.createElement('script')
  cordovaScript.setAttribute('type', 'text/javascript')
  cordovaScript.setAttribute('src', 'cordova.js')
  document.body.appendChild(cordovaScript)
}

/* eslint-disable no-new */
//    new Vue({
//      el: '#app',
//      template: '<App/>',
//      components: { App },
//      data: function () {
//        return {
//          cordova: Vue.cordova
//        }
//      }
//    })

Vue.use(Vuetify, {
  theme: {
    primary: '#37474f',
    secondary: '#455a64',
    primaryFont: '#cfd8dc',
    secondaryFont: '#90a4ae',
    accent: '#ff9800',
    darken: 'rgb(40, 40, 40)',
    error: '#b71c1c'
  }
})

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAe-trRSweGmcIFspvfEw9Aqpfkr_xYhiU',
    libraries: 'places'
  }
})

const veeConfig = {
  strict: false
}

Vue.use(VeeValidate, veeConfig)

var v = new Vue({
  el: '#app',
  router,
  store,
  template: '<NewVue/>',
  components: {
    NewVue
  },
  data: function () {
    return {
      cordova: Vue.cordova,
      message: 'Passed in from main.js'
    }
  }
})

setTimeout(function () {
  NewVue.message = 'test2'
  v.data = function () {
    return {
      cordova: Vue.cordova,
      message: 'Passed in from main.js'
    }
  }
}, 1000)

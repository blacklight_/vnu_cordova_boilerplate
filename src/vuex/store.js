import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
Vue.use(axios)

let api = axios.create({
  headers: {
    common: {
      'x-access-token': window.localStorage.token
    //  'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZXNzaW9uSWQiOiIxMjYwODdiMC01ZDkwLTExZTgtODMyNy0zYzRlZTRkYjQ4NTBfOHpMMFZmeE52b0lCbVR5aXlQWWNxekpxUWRWMUJJcWhiaUFOaG1YTjc3RjZsYWowR2oxUmZ1MVprQWxudHE0K1Q1bUFmZTBueXhDdkVZUnZpOXQ5TEE9PSIsImlhdCI6MTUzMTgwNjUwMywiZXhwIjoxNTM5NTgyNTAzfQ.Ex-RgOyknJQt4cu-iokCqfhkTlmOxTY3qcbldyTuypQ'
    },
    post: {
      'x-access-token': window.localStorage.token
    //  'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZXNzaW9uSWQiOiIxMjYwODdiMC01ZDkwLTExZTgtODMyNy0zYzRlZTRkYjQ4NTBfOHpMMFZmeE52b0lCbVR5aXlQWWNxekpxUWRWMUJJcWhiaUFOaG1YTjc3RjZsYWowR2oxUmZ1MVprQWxudHE0K1Q1bUFmZTBueXhDdkVZUnZpOXQ5TEE9PSIsImlhdCI6MTUzMTgwNjUwMywiZXhwIjoxNTM5NTgyNTAzfQ.Ex-RgOyknJQt4cu-iokCqfhkTlmOxTY3qcbldyTuypQ'
    }
  }
})

const state = {
  serverUrl: 'http://localhost:3001',
  gridview: true,
  globalUserInfo: {
    userInfo: {},
    hostInfo: {},
    bandInfo: {}
  },
  auth: {
    userStatus: false,
    hostStatus: false,
    bandStatus: false
  },
  testUser: {
    avatar: 'https://f4.bcbits.com/img/0007337486_10.jpg',
    name: 'Southpaw',
    genre: 'metalcore',
    followers: '56'
  },
  vnuConnect: {
    main: {
      count: 0,
      data: []
    },
    sub: {
      count: 0,
      data: null
    }
  },
  shows: {},
  userLogin: {
    jumbo: 'https://images.pexels.com/photos/247789/pexels-photo-247789.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb',
    tag: 'Simplifying Shows',
    type: 'userLogin',
    mission: 'VNU connects local talent with nearby venues',
    data: [
      {
        text: 'Login',
        help: 'Don\'t have an account with us yet?',
        forms: [
          { type: 'text-field', text: 'email', vvName: 'email', veeValidate: 'required|email' },
          { type: 'password', text: 'password', vvName: 'password', veeValidate: 'required' }
        ]
      }
    ],
    points: [
        { icon: 'attach_money', title: 'rewarding', text: 'With VNU, the vast majority of proceeds go to the host and artists playing. We aim to pay musicians at least twice that of traditional promoters and hosts hundreds of dollars per show.' },
        { icon: 'mood', title: 'helpful', text: 'VNU creates shows for you, we’ll never book one without your permission, or jeopardize your integrity. You tell us the days you’d like to play or host, the genres you like, and we’ll show you local opportunities that pay the bills and support your scene.' },
        { icon: 'linear_scale', title: 'easy', text: 'Music isn’t about endless networking, logistics, or negotiations. VNU handles the tedious tasks, making it easy for just about anyone to host or play a show.' }
    ]
  },
  userReg: {
    jumbo: 'https://images.pexels.com/photos/247789/pexels-photo-247789.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb',
    tag: 'Simplifying Shows',
    type: 'userReg',
    mission: 'VNU connects local talent with nearby venues',
    data: [
      {
        text: 'Register',
        help: 'Have an account?',
        forms: [
          { type: 'text-field', text: 'email', vvName: 'email', veeValidate: 'required|email' },
          { type: 'password', text: 'password', vvName: 'password', veeValidate: 'required' }
        ]
      }
    ],
    points: [
        { icon: 'attach_money', title: 'rewarding', text: 'With VNU, the vast majority of proceeds go to the host and artists playing. We aim to pay musicians at least twice that of traditional promoters and hosts hundreds of dollars per show.' },
        { icon: 'mood', title: 'helpful', text: 'VNU creates shows for you, we’ll never book one without your permission, or jeopardize your integrity. You tell us the days you’d like to play or host, the genres you like, and we’ll show you local opportunities that pay the bills and support your scene.' },
        { icon: 'linear_scale', title: 'easy', text: 'Music isn’t about endless networking, logistics, or negotiations. VNU handles the tedious tasks, making it easy for just about anyone to host or play a show.' }
    ]
  },
  actReg: {
    title: 'Find shows',
    helper: 'Looking to create an event?',
    helperIcon: 'info_outline',
    data: [
      {
        title: 'Create a simple profile (should only take a few minutes)',
        text: 'Let promoters and know a bit about yourself and your music.',
        forms: [
          { type: 'text-field', text: 'Name of your band', veeValidate: 'required', alias: 'name' },
          { type: 'text-field', text: 'Genre', veeValidate: 'required', alias: 'genre' },
          { type: 'textArea', text: 'Description', veeValidate: 'required', alias: 'description' },
          { type: 'text-field', text: 'What equipment can you provide? Ask carter or alec for list', veeValidate: 'required', alias: 'fb link' },
          { type: 'textArea', text: 'Link a fb page', veeValidate: '', alias: 'fb link' }
        ]
      },
      {
        title: 'Upload a profile picture',
        text: 'Let promoters and know a bit about yourself and your music.',
        forms: [
          { type: 'text-field', text: 'Upload a profile/cover photo', veeValidate: 'required', alias: 'picture' }
        ]
      },
      {
        type: 'Payment',
        title: 'Awesome. How would you like to be paid?',
        text: 'Last step. Just confirm your ID and tell us how you\'d like to be paid!',
        forms: []
      }
    ]
  },
  hostReg: {
    title: 'Create an Event',
    helper: 'Looking to play a gig?',
    helperIcon: 'info_outline',
    data: [
      {
        title: 'Event Description',
        text: 'Think tinder, escept you\'re not trying to get laid.',
        forms: [
          { type: 'text-field', text: 'Name your event', veeValidate: 'required', alias: 'event' },
          { type: 'textArea', text: 'What\'s your event about?', veeValidate: 'required', alias: 'description' },
          { type: 'text-field', text: 'How many tickets do you want to issue?', veeValidate: 'required|numeric', alias: 'ticket' }
        ]
      },
      {
        title: 'Event Details',
        text: 'We just need a little info first. Shouldn\'t take more than ten minutes',
        buttons: [
          { text: 'Done' },
          { text: 'Go back' }
        ],
        forms: [
          { type: 'text-field', text: 'Street Address', veeValidate: 'required', alias: 'address' },
          { type: 'text-field', text: 'Postal Code', veeValidate: 'required', alias: 'postal code' },
          { type: 'select', text: 'Venue Type', options: ['private residence', 'bar or club', 'music hall'], veeValidate: 'required', alias: 'venue tyoe' },
          { type: 'date-picker', text: 'Event Date', veeValidate: 'required', alias: 'date' },
          { type: 'time-picker', text: 'Event Time', veeValidate: 'required', alias: 'time' }
        ]
      },
      {
        type: 'Payment',
        title: 'Awesome. How would you like to be paid?',
        text: 'Last step. Just confirm your ID and tell us how you\'d like to be paid!',
        forms: []
      }
    ]
  },
  mainSteps: [ // Add a 'why host a show through vnu' button?
    {
      toggle: []
    },
    {
      header: 'Host or Play with VNU',
      type: 'toggle',
      msg: 'Would you prefer to host or play?',
      buttons: [
        {
          text: 'Host'
        },
        {
          text: 'Play'
        }
      ]
    },
    {
      header: 'Upcoming opportunities',
      toggle: [
        {
          type: 'picker', // We should live display results below
          text: 'Looking for a certain date?',
          buttons: [
            { text: 'search' }
          ]
        }
      ]
    }
  ],
  openedObject: {
    vis: false,
    data: {}
  },
  showsByDate: [
    {
      time: 'Your Shows',
      show: [
        {
          content: true,
          avatar: 'http://ottawa.carpediem.cd/data/afisha/o/ab/5d/ab5d3a4ca4.jpg',
          title: 'Leaky A\'s',
          acts: [
            {avatar: 'https://f4.bcbits.com/img/0007337486_10.jpg', name: 'Southpaw', genre: 'metalcore'},
            {avatar: 'http://newnoisemagazine.com/wp-content/uploads/2016/08/Horcrux-band-2016.jpg', name: 'Horocrux', genre: 'huffle puff metal'},
            {avatar: 'https://i0.wp.com/www.martyrmagazine.ca/wp-content/uploads/2014/04/deaner_web-53-e1398821064286.jpg', name: 'Between the Shity Bands and Us', genre: 'metal'}
          ],
          genre: 'Metal',
          doors: '6:00pm',
          age: '16',
          location: 'Around Bridlewood',
          price: '$17',
          date: new Date(2018, 10, 30)
        },
        {
          content: true,
          avatar: 'http://ottawa.carpediem.cd/data/afisha/o/fd/82/fd821b8135.jpg',
          title: 'Leaky B\'s',
          acts: [
            {avatar: 'https://f4.bcbits.com/img/0007337486_10.jpg', name: 'Southpaw', genre: 'metalcore'},
            {avatar: 'http://newnoisemagazine.com/wp-content/uploads/2016/08/Horcrux-band-2016.jpg', name: 'Horocrux', genre: 'huffle puff metal'},
            {avatar: 'https://i0.wp.com/www.martyrmagazine.ca/wp-content/uploads/2014/04/deaner_web-53-e1398821064286.jpg', name: 'Between the Shity Bands and Us', genre: 'metal'}
          ],
          genre: 'Metal',
          doors: '6:00pm',
          age: '16',
          location: 'Around Bridlewood',
          price: '$17',
          date: new Date(2018, 10, 30)
        },
        {
          content: true,
          avatar: 'http://www.slate.com/content/dam/slate/blogs/the_vault/2015/folder/20/TajMahal.jpg.CROP.original-original.jpg',
          title: 'Leaky C\'s',
          acts: [
            {avatar: 'https://f4.bcbits.com/img/0007337486_10.jpg', name: 'Southpaw', genre: 'metalcore'},
            {avatar: 'http://newnoisemagazine.com/wp-content/uploads/2016/08/Horcrux-band-2016.jpg', name: 'Horocrux', genre: 'huffle puff metal'},
            {avatar: 'https://i0.wp.com/www.martyrmagazine.ca/wp-content/uploads/2014/04/deaner_web-53-e1398821064286.jpg', name: 'Between the Shiity Bands and Us', genre: 'metal'}
          ],
          genre: 'Metal',
          doors: '6:00pm',
          age: '16',
          location: 'Around Bridlewood',
          price: '$17',
          date: new Date(2018, 10, 28)
        }
      ]
    },
    {
      time: 'Local Shows Tomorrow',
      show: [
        {
          content: true,
          avatar: 'https://images.stubsites.com/webassets.ticketmob.com/ES/images/ShowPosters/6D808D83-FE00-5334-0879C10A375CA58E.jpg',
          title: 'Leaky C\'s',
          acts: [
            {avatar: 'https://f4.bcbits.com/img/0007337486_10.jpg', name: 'Southpaw', genre: 'metalcore'},
            {avatar: 'http://newnoisemagazine.com/wp-content/uploads/2016/08/Horcrux-band-2016.jpg', name: 'Horocrux', genre: 'huffle puff metal'},
            {avatar: 'https://i0.wp.com/www.martyrmagazine.ca/wp-content/uploads/2014/04/deaner_web-53-e1398821064286.jpg', name: 'Between the Shiity Bands and Us', genre: 'metal'}
          ],
          genre: 'Metal',
          doors: '6:00pm',
          age: '16',
          location: 'Around Bridlewood',
          price: '$17',
          date: '27/01/2018'
        },
        {
          content: true,
          avatar: 'https://images.stubsites.com/webassets.ticketmob.com/ES/images/ShowPosters/0FD38511-984F-3704-AD015389841621DB.jpg',
          title: 'Leaky C\'s',
          acts: [
            {avatar: 'https://f4.bcbits.com/img/0007337486_10.jpg', name: 'Southpaw', genre: 'metalcore'},
            {avatar: 'http://newnoisemagazine.com/wp-content/uploads/2016/08/Horcrux-band-2016.jpg', name: 'Horocrux', genre: 'huffle puff metal'},
            {avatar: 'https://i0.wp.com/www.martyrmagazine.ca/wp-content/uploads/2014/04/deaner_web-53-e1398821064286.jpg', name: 'Between the Shity Bands and Us', genre: 'metal'}
          ],
          genre: 'Metal',
          doors: '6:00pm',
          age: '16',
          location: 'Around Bridlewood',
          price: '$17',
          date: '27/01/2018'
        },
        {
          content: true,
          avatar: 'https://www.ticketscene.ca/uploads/event19155.jpg',
          title: 'Leaky C\'s',
          acts: [
            {avatar: 'https://f4.bcbits.com/img/0007337486_10.jpg', name: 'Southpaw', genre: 'metalcore'},
            {avatar: 'http://newnoisemagazine.com/wp-content/uploads/2016/08/Horcrux-band-2016.jpg', name: 'Horocrux', genre: 'huffle puff metal'},
            {avatar: 'https://i0.wp.com/www.martyrmagazine.ca/wp-content/uploads/2014/04/deaner_web-53-e1398821064286.jpg', name: 'Between the Shiity Bands and Us', genre: 'metal'}
          ],
          genre: 'Metal',
          doors: '6:00pm',
          age: '16',
          location: 'Around Bridlewood',
          price: '$17',
          date: '27/01/2018'
        }
      ]
    },
    {
      time: 'Local Shows this Month',
      show: [
        {
          content: true,
          avatar: '',
          title: 'Leaky C\'s',
          acts: [
            {avatar: 'https://f4.bcbits.com/img/0007337486_10.jpg', name: 'Southpaw', genre: 'metalcore'},
            {avatar: 'http://newnoisemagazine.com/wp-content/uploads/2016/08/Horcrux-band-2016.jpg', name: 'Horocrux', genre: 'huffle puff metal'},
            {avatar: 'https://i0.wp.com/www.martyrmagazine.ca/wp-content/uploads/2014/04/deaner_web-53-e1398821064286.jpg', name: 'Between the Shiity Bands and Us', genre: 'metal'}
          ],
          genre: 'Metal',
          doors: '6:00pm',
          age: '16',
          location: 'Around Bridlewood',
          price: '$17',
          date: '27/01/2018'
        },
        {
          content: true,
          avatar: 'http://www.slate.com/content/dam/slate/blogs/the_vault/2015/folder/20/TajMahal.jpg.CROP.original-original.jpg',
          title: 'Leaky C\'s',
          acts: [
            {avatar: 'https://f4.bcbits.com/img/0007337486_10.jpg', name: 'Southpaw', genre: 'metalcore'},
            {avatar: 'http://newnoisemagazine.com/wp-content/uploads/2016/08/Horcrux-band-2016.jpg', name: 'Horocrux', genre: 'huffle puff metal'},
            {avatar: 'https://i0.wp.com/www.martyrmagazine.ca/wp-content/uploads/2014/04/deaner_web-53-e1398821064286.jpg', name: 'Between the Shiity Bands and Us', genre: 'metal'}
          ],
          genre: 'Metal',
          doors: '6:00pm',
          age: '16',
          location: 'Around Bridlewood',
          price: '$17',
          date: '27/01/2018'
        },
        {
          content: true,
          avatar: 'http://www.slate.com/content/dam/slate/blogs/the_vault/2015/folder/20/TajMahal.jpg.CROP.original-original.jpg',
          title: 'Leaky C\'s',
          acts: [
            {avatar: 'https://f4.bcbits.com/img/0007337486_10.jpg', name: 'Southpaw', genre: 'metalcore'},
            {avatar: 'http://newnoisemagazine.com/wp-content/uploads/2016/08/Horcrux-band-2016.jpg', name: 'Horocrux', genre: 'huffle puff metal'},
            {avatar: 'https://i0.wp.com/www.martyrmagazine.ca/wp-content/uploads/2014/04/deaner_web-53-e1398821064286.jpg', name: 'Between the Shiity Bands and Us', genre: 'metal'}
          ],
          genre: 'Metal',
          doors: '6:00pm',
          age: '16',
          location: 'Around Bridlewood',
          price: '$17',
          date: '27/01/2018'
        }
      ]
    }
  ]
}

const actions = {
  get_shows ({ commit }) {
    api.get('http://localhost:3001/shows/recent').then(function (response) { console.log('This is the serve response' + JSON.stringify(response, null, 4)); commit('GET_SHOWS', response) }).catch(function (error) { console.log(error) })
  },
  login (context, payload) {
    api.post('http://localhost:3001/auth/login', payload).then(function (response) { console.log('This is the server response' + JSON.stringify(response, null, 4)); context.commit('SAVE_SESSION', response) }).catch(function (error) { console.log(error) })
  },
  get_act ({ commit }) {
    const actid = '74529300-d4c3-11e8-aaaa-f96bc169242d'
    api.get(state.serverUrl + '/acts/' + actid, { params: { token: window.localStorage.token } }).then(function (response) { console.log(response) }).catch(function (error) { console.log(error) })
  },
  submit_show (context, payload) {
    api.post('http://localhost:3001/shows/new', payload).then(function (response) { console.log('This is the server response' + JSON.stringify(response, null, 4)); context.commit('SUBMIT_SHOW', response) }).catch(function (error) { console.log(error) })
  },
  submit_act (context, payload) {
    api.post('http://localhost:3001/acts/new', payload).then(function (response) { console.log('This is the server response' + JSON.stringify(response, null, 4)); context.commit('SUBMIT_ACT', response) }).catch(function (error) { console.log(error) })
  },
  submit_user (context, payload) {
    api.post('http://localhost:3001/users/new', payload).then(function (response) { console.log('This is the server response' + JSON.stringify(response, null, 4)); context.commit('SUBMIT_USER', response) }).catch(function (error) { console.log(error) })
  },
  show_openedObject (context, payload) {
    context.commit('SHOW_OPENED_OBJECT', payload)
  },
  hide_openedObject (context) {
    context.commit('HIDE_OPENED_OBJECT')
  },
  load_vnuConnect_main (context, payload) {
    context.commit('LOAD_VNUCONNECT_MAIN', payload)
  },
  load_vnuConnect_sub (context, payload) {
    context.commit('LOAD_VNUCONNECT_SUB', payload)
  },
  close_vnuConnect (context) {
    context.commit('CLOSE_VNUCONNECT')
  },
  increment_vnuConnect_main (context) {
    context.commit('INCREMENT_VNUCONNECT_MAIN')
  },
  decrement_vnuConnect_main (context) {
    context.commit('DECREMENT_VNUCONNECT_MAIN')
  },
  increment_vnuConnect_sub (context) {
    context.commit('INCREMENT_VNUCONNECT_SUB')
  },
  decrement_vnuConnect_sub (context) {
    context.commit('DECREMENT_VNUCONNECT_SUB')
  },
  store_user_info (context, payload) {
    context.commit('STORE_USER_INFO', payload)
  },
  user_auth (context, payload) {
    context.commit('USER_AUTH', payload)
  },
  change_gridview (context, payload) {
    context.commit('CHANGE_GRIDVIEW', payload)
  }
}

const mutations = {
  GET_SHOWS (state, payload) { // This sorts all of the retrieved shows into an array of arrays to better suit the collapsable sections of the feed
    var showList = []
    console.log('This is what arr is within GET_SHOWS: ' + JSON.stringify(payload.data, 4))

    var currentTime = helpers.formatDate(currentTime, -1)
    var today = helpers.formatDate(today, 0)
    var tomorrow = helpers.formatDate(tomorrow, 1)
    var week = helpers.formatDate(week, 7)
    var month = helpers.formatDate(month, 30)

    showList[0] = {title: 'Today', time: today, show: []}
    showList[1] = {title: 'Tomorrow', time: tomorrow, show: []}
    showList[2] = {title: 'This week', time: week, show: []}
    showList[3] = {title: 'This month', time: month, show: []}

    showList.forEach((currentList, index) => {
      showList[index] = helpers.sortShows(currentTime, currentList, payload.data)
    })
    console.log('This is arr just before  it\'s returned from fillShowList: ' + JSON.stringify(payload.data, 4))
    state.shows = showList
  },
  SAVE_SESSION (state, payload) {
    console.log('res.token: ' + payload.data.token)
    window.localStorage.token = payload.data.token
    console.log('window.localStorage.token : ' + window.localStorage.token)
  },
  FORGET_SESSION (state, payload) {
  },
  SUBMIT_USER (state, payload) {
  },
  SUBMIT_ACT (state, payload) {
  },
  SUBMIT_SHOW (state, payload) {
  },
  SHOW_OPENED_OBJECT (state, payload) {
    state.openedObject.vis = true
    state.openedObject.data = payload
  },
  HIDE_OPENED_OBJECT (state) {
    state.openedObject.vis = false
    state.openedObject.data = null
  },
  LOAD_VNUCONNECT_MAIN (state, payload) {
    state.vnuConnect.main.data = payload
  },
  LOAD_VNUCONNECT_SUB (state, payload) {
    state.vnuConnect.sub.data = payload
  },
  CLOSE_VNUCONNECT (state) {
    state.vnuConnect.main.data = null
    state.vnuConnect.sub.data = null
  },
  INCREMENT_VNUCONNECT_MAIN (state) {
    state.vnuConnect.main.count++
  },
  DECREMENT_VNUCONNECT_MAIN (state) {
    state.vnuConnect.main.count--
  },
  INCREMENT_VNUCONNECT_SUB (state) {
    state.vnuConnect.sub.count++
  },
  DECREMENT_VNUCONNECT_SUB (state) {
    state.vnuConnect.sub.count--
  },
  STORE_USER_INFO (state, payload) {
    state.globalUserInfo = payload
  },
  USER_AUTH (state, payload) {
    state.auth.userStatus = payload
  },
  CHANGE_GRIDVIEW (state, payload) {
    state.gridview = payload
  }
}

const getters = {
  openedObjectVis: state => {
    return state.openedObject.vis
  },
  shows: state => {
    return state.shows
  },
  openedObjectData: state => {
    return state.openedObject.data
  },
  vnuConnectMain: state => {
    return state.vnuConnect.main
  },
  vnuConnectSub: state => {
    return state.vnuConnect.sub
  },
  getGlobalUserInfo: state => {
    return state.globalUserInfo
  },
  getAuthStatues: state => {
    return state.auth
  },
  getGridview: state => {
    return state.gridview
  },
  token: state => {
    return state.token
  }
}

const helpers = {
  sortShows (currentTime, currentList, arr) {
    console.log('DDDDDDDDDDDDDDDDDDDD this is arr as its passed in: ' + JSON.stringify(arr, 4))
    console.log('DDDDDDDDDDDDDDDDDDDD this is current time: ' + JSON.stringify(currentTime, 4))
    arr.forEach((show, index) => {
      console.log('HHHHHHHHHHHHHHH Show is : ' + JSON.stringify(show, 4))
      if (show === null || show === undefined) return
      else if (new Date(show.date).getTime() > currentTime.getTime() && new Date(show.date).getTime() < currentList.time.getTime()) {
        currentList.show[index] = show
        console.log('OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO This is show before being pushed onto list: ' + JSON.stringify(show, 4))
        arr[index] = null
        console.log('This is arr: ' + JSON.stringify(arr))
        console.log('This is currentlist.show: ' + JSON.stringify(currentList.show))
      }
    })
    console.log('DDDDDDDDDDDDDDDDD This is currentlist: ' + JSON.stringify(currentList))
    return currentList
  },
  formatDate: function (date, num) {
    date = new Date()
    date.setUTCHours(0, 0, 0, 0)// Set to midnight
    date.setDate(date.getDate() + 1 + num)
    return date
  }
}

export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters,
  helpers
})

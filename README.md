#VNU boilerplate

Template for starting out vnu dev

Read the README-Orig.md for instructions for setting up, I will put a summary below:

## How-to
### Install NPM and nodejs on the machine you are developing on
windows: [here](https://nodejs.org/en/download/)
linux: [here](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions)
I recommend just making a ubuntu vm 


**Install NPM dependencies and build static files**

``` bash
npm install
```

**Test in iOS**

``` bash
npm run ios
```

**Test in browser**

``` bash
npm run browser
```

## Where to start developing

You can start making your template by writing your html stuff in `src/newVue.vue`
I'm not exactly sure how all the cordove node js stuff works, but the js file that is running in
browser is `src/main.js`

To see an example of how css is used in a vue look at `src/App.vue` with was the original landing page

You will need to keep a terminal running with the `npm run browser` which will open up
the autorefreshing browser thing and also show webpack's compiling errors. I prefer to
use the terminal built into netbeans so it's all in one place

I don't really have a solution for netbeans not recognizing the .vue filetype yet
